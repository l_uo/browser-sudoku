# Browser-sudoku
	minimal sudoku game for web browsers.

![example](img/output.gif "Title Text")

# Installation & running
```console
   npx shadow-cljs release app
   mkdir -p target && cp assets/index.html target/
   npx serve
```
