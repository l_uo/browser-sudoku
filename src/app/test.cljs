(ns app.test
  (:require [app.lib :as lib]
            [app.main :as main]
            [cljs.test :refer [deftest is]]))

(def test-board [5 3 0 0 7 0 0 0 0
                 6 0 0 1 9 5 0 0 0
                 0 9 8 0 0 0 0 6 0
                 8 0 0 0 6 0 0 0 3
                 4 0 0 8 0 3 0 0 1
                 7 0 0 0 2 0 0 0 6
                 0 6 0 0 0 0 2 8 0
                 0 0 0 4 1 9 0 0 5
                 0 0 0 0 8 0 0 7 9])

(deftest test-group
  (is (= [2 8 0 0 0 5 0 7 9]
         (lib/get-group test-board 80))))

(deftest test-col
  (is (= [5 6 0 8 4 7 0 0 0]
         (lib/get-col test-board 0))))

(deftest test-row
  (is (= [8 0 0 0 6 0 0 0 3]
         (lib/get-row test-board 27))))

(deftest test-possible-values
  (is (= #{1 2}
         (set (lib/possible-values test-board 0)))))

(deftest test-all-positions
  (let [{:keys [board]} (lib/solve test-board)
        f (apply every-pred (for [i (range 81)]
                                 #(= 0 (count (lib/possible-values % i)))))]
    (is (= true
           (f board)))))

