(ns app.lib
  (:require [clojure.set :as s]))

(defn get-row [board pos]
  (let [row (quot pos 9)]
    (for [i (range 9)]
      (get board (+ i (* row 9))))))

(defn get-col [board pos]
  (let [col (mod pos 9)]
    (for [i (range 9)]
      (get board (+ col (* i 9))))))

(defn get-group [board pos]
  (let [group-row (quot pos 27)
        group-col (quot (mod pos 9) 3)
        left-up-group-pos (+ (* 3 group-col)
                             (* 27 group-row))]
    (apply concat (for [x (range 3)]
                    (for [i (range 3)]
                      (get board (+ i left-up-group-pos (* x 9))))))))

(defn valid-move? [board pos]
  (let [pos-value (get board pos)
        more-than-one? (fn [f] (< 1 (count (filter #(= % pos-value)
                                                   (f board pos)))))]
    (cond
      (zero? pos-value) true
      (more-than-one? get-row) false
      (more-than-one? get-col) false
      (more-than-one? get-group) false
      :else true)))

(defn possible-values
  "finds all the possible values for index n in board"
  [board n]
  (let [rows (get-row board n)
        cols (get-col board n)
        group (get-group board n)
        pos-vals (apply s/union (map set [rows cols group]))]
    (shuffle (vec (s/difference #{1 2 3 4 5 6 7 8 9} pos-vals)))))

(defn solve
  "usage: (solve (create-board))"
  ([board]
   (let [timer (.getTime (js/Date.))]
     (solve {:solvable? false :board board :start-board board}
            0
            timer)))
  ([{:keys [solvable? board] :as all} n timer]
   (cond
     (>= n 81) (assoc all :solvable? true)
     (not (zero? (get board n))) (solve all (inc n) timer)
     :else (loop [[v & vs] (possible-values board n)]
             (cond
               (<= 1500 (- (.getTime (js/Date.))
                           timer))
               all
               (nil? v) false
               :else (if-let [new-board (solve (assoc all :board
                                                      (assoc board n v))
                                               (inc n)
                                               timer)]
                       new-board
                       (recur vs)))))))

(defn solve2
  "usage: (solve (create-board))"
  ([board]
     (solve2 board 0))
  ([board n]
   (cond
     (>= n 81) board
     (not (zero? (get board n))) (solve2 board (inc n))
     :else (loop [[v & vs] (possible-values board n)]
             (cond
               (nil? v) false
               :else (if-let [new-board (solve2 (assoc board n v) (inc n))]
                       new-board
                       (recur vs)))))))

(defn create-board-candidate
  ([] (let [board (vec (take 81 (repeat 0)))
            positions (take 17 (shuffle (range 81)))]
        (create-board-candidate board positions)))
  ([board positions]
   (cond
     (empty? positions) board
     :else (loop [[cur-pos & pos] positions
                  [v & vs] (possible-values board cur-pos)]
             (cond
               (not v) false
               (and v cur-pos) (if-let [new-board (create-board-candidate
                                                   (assoc board cur-pos v)
                                                   pos)]
                                 new-board
                                 (recur positions vs)))))))

(defn create-board []
  (let [solved-board (solve (into [] (repeat 81 0)))
        {:keys [solvable? board]} solved-board]
    (if solvable?
      (reduce (fn [board position] (assoc-in board [:board position] 0))
       {:board board :solved-board board}
       (take 64 (shuffle (range 81))))
      (create-board))))
