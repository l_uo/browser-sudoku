(ns app.main
  (:require [app.lib :as lib]
            [reagent.core :as r]
            [reagent.dom :refer [render]]
            [cljs.core.async :as async]))

(def state (r/atom (lib/create-board)))
(def event-queue (async/chan))

(async/go-loop [[event pos] (async/<! event-queue)]
  (case event
    :inc (swap! state update-in [:board pos] #(mod (inc %) 10))
    :new-game (reset! state (lib/create-board))
    :reveal-solution (swap! state assoc-in [:board] (:solved-board @state)))
  (recur (async/<! event-queue)))

(defn grid []
  (into [:div {:class "grid-container"}]
        (for [row (range 9)
              col (range 9)
              :let [pos (+ col (* row 9))
                    color (when-not (lib/valid-move? (:board @state) pos)
                            "tomato")
                    pos-value (get-in @state [:board pos])]]
          [:button
           {:class pos
            :style {:backgroundColor color
                    :color (when (zero? pos-value)
                             "lightblue")
                    :margin-right (when (or (= col 2) (= col 5))
                                    "10px")
                    :margin-bottom (when (or (= row 2) (= row 5))
                                     "10px")}
            :on-click #(async/put! event-queue [:inc pos])}
             pos-value])))

(defn new-game-component []
  [:button
   {:class "game-button"
    :id "new-game"
    :on-click #(async/put! event-queue [:new-game])}
   "New game"])

(defn reveal-solution-component []
  [:button
   {:class "game-button"
    :id "reveal-solution"
    :on-click #(async/put! event-queue [:reveal-solution])}
   "Reveal game"])

(defn root-component []
  [:div {:class "flex-container"}
   (grid)
   [:div (new-game-component) (reveal-solution-component)]])

(defn render-new-game []
  (render
   [new-game-component]
   (.appendChild js/document "app")))

(defn render-root []
  (render
   [root-component]
   (.getElementById js/document "app")))

(defn reload! []
  (render-root)
  (println "[main] reloaded"))

(defn main! []
  (render-root)
  (println "[main]: loading"))
